resource "aws_eip" "nat_gw_eip" {
  vpc      = true
}

resource "aws_nat_gateway" "private_subnet_natgw" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.public_subnet.id

  tags = {
    Name = "aug-vpc-natgw"
  }

  depends_on = [aws_internet_gateway.aug_22_igw]
}
