resource "aws_internet_gateway" "aug_22_igw" {
  vpc_id = aws_vpc.aug_vpc.id

  tags = {
    Name = "aug-22-vpc-igw"
  }
}
